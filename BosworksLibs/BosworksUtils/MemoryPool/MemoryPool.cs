﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.MemoryPool
{
    public interface IBufferPool<T>
    {
        void Attach(BufferAllocator<T> methodTable);
    }

    public class BufferAllocator<TBufType>
    { 
        public Func<int, TBufType> AllocBuffer = null;
        public Func<TBufType, bool> ReleaseBuffer = null;
        public Func<TBufType, int, ArraySegment<byte>> ReallocBuffer = null;
        public Func<TBufType, ArraySegment<byte>, int, int, int> WriteBuffer = null;
        public Func<TBufType, ArraySegment<byte>, int, int, int> ReadBuffer = null;
    }

  
    public static class ArraySegPool 
    {
        public static ArraySegment<byte> AllocBuffer(int amount)
        {
            var res = new byte[amount];
            return new ArraySegment<byte>(res);
        }

        public static bool ReleaseBuffer(ArraySegment<byte> buf)
        {
            return true;
        }

        public static int WriteBuffer(ArraySegment<byte> src, ArraySegment<byte> dest, int writeIdx, int amount)
        {
            Buffer.BlockCopy(src.Array, src.Offset, dest.Array, dest.Offset + writeIdx, amount);
            return amount;            
        }

        public static int ReadBuffer(ArraySegment<byte> src, ArraySegment<byte> dest, int readIdx, int amount)
        {
            Buffer.BlockCopy(dest.Array, dest.Offset, src.Array, src.Offset + readIdx, amount);
            return amount;
        }

        public static ArraySegment<byte> ReallocBuffer(ArraySegment<byte> src, int newLen)
        {
            var newMem = AllocBuffer(newLen);
            Buffer.BlockCopy(src.Array, src.Offset, newMem.Array, newMem.Offset, src.Count);
            return newMem;            
        }

        public static void Attach(BufferAllocator<ArraySegment<byte>>  methodTable)
        {
            methodTable.AllocBuffer = AllocBuffer;
            methodTable.ReleaseBuffer = ReleaseBuffer;
            methodTable.ReallocBuffer = ReallocBuffer;
            methodTable.WriteBuffer = WriteBuffer;
            methodTable.ReadBuffer = ReadBuffer;
        }
    }
}
