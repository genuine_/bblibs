﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Diagnostics.CodeAnalysis;
using System.IO;

/* Bsb - I stole this from someplace. Very handy. */
namespace ArraySegments
{
    public static class ArraySegmentExtensions
    {
        
        public static ArraySegment<T> AsArraySegment<T>(this T[] array, int offset, int count)
        {
            Contract.Requires(array != null);
            Contract.Requires(offset >= 0);
            Contract.Requires(offset <= array.Length);
            Contract.Requires(count >= 0);
            Contract.Requires(count <= array.Length - offset);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Array == array);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Offset == offset);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Count == count);
            return new ArraySegment<T>(array, offset, count);
        }

        public static ArraySegment<T> AsArraySegment<T>(this T[] array, int offset = 0)
        {
            Contract.Requires(array != null);
            Contract.Requires(offset >= 0);
            Contract.Requires(offset <= array.Length);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Array == array);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Offset == offset);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Count == array.Length - offset);
            return new ArraySegment<T>(array, offset, array.Length - offset);
        }
 
        public static MemoryStream CreateStream(this ArraySegment<byte> segment, bool writable = true)
        {
            Contract.Ensures(Contract.Result<MemoryStream>() != null);
            return new MemoryStream(segment.Array, segment.Offset, segment.Count, writable);
        }
 
        public static BinaryReader CreateBinaryReader(this ArraySegment<byte> segment)
        {
            Contract.Ensures(Contract.Result<BinaryReader>() != null);
            return new BinaryReader(segment.CreateStream(false));
        }
 
        public static BinaryWriter CreateBinaryWriter(this ArraySegment<byte> segment)
        {
            Contract.Ensures(Contract.Result<BinaryWriter>() != null);
            return new BinaryWriter(segment.CreateStream(true));
        }
 
        public static ArraySegmentReader<T> CreateArraySegmentReader<T>(this ArraySegment<T> segment)
        {
            Contract.Ensures(Contract.Result<ArraySegmentReader<T>>() != null);
            Contract.Ensures(Contract.Result<ArraySegmentReader<T>>().Source == segment);
            return new ArraySegmentReader<T>(segment);
        }
        
  
        public static ArraySegment<T> Take<T>(this ArraySegment<T> segment, int count)
        {
            Contract.Requires(count >= 0);
            Contract.Requires(count <= segment.Count);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Array == segment.Array);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Offset == segment.Offset);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Count == count);
            return new ArraySegment<T>(segment.Array, segment.Offset, count);
        }

        public static ArraySegment<T> Skip<T>(this ArraySegment<T> segment, int count)
        {
            Contract.Requires(count >= 0);
            Contract.Requires(count <= segment.Count);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Array == segment.Array);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Offset == segment.Offset + count);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Count == segment.Count - count);
            return new ArraySegment<T>(segment.Array, segment.Offset + count, segment.Count - count);
        }
 
        public static ArraySegment<T> Slice<T>(this ArraySegment<T> segment, int skipCount, int takeCount)
        {
            Contract.Requires(skipCount >= 0);
            Contract.Requires(skipCount <= segment.Count);
            Contract.Requires(takeCount >= 0);
            Contract.Requires(takeCount <= segment.Count - skipCount);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Array == segment.Array);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Offset == segment.Offset + skipCount);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Count == takeCount);
            return new ArraySegment<T>(segment.Array, segment.Offset + skipCount, takeCount);
        }
 
        public static ArraySegment<T> TakeLast<T>(this ArraySegment<T> segment, int count)
        {
            Contract.Requires(count >= 0);
            Contract.Requires(count <= segment.Count);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Array == segment.Array);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Offset == segment.Offset + segment.Count - count);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Count == count);
            return segment.Skip(segment.Count - count);
        }
 
        public static ArraySegment<T> SkipLast<T>(this ArraySegment<T> segment, int count)
        {
            Contract.Requires(count >= 0);
            Contract.Requires(count <= segment.Count);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Array == segment.Array);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Offset == segment.Offset);
            Contract.Ensures(Contract.Result<ArraySegment<T>>().Count == segment.Count - count);
            return segment.Take(segment.Count - count);
        }
 
        public static ArraySegmentListWrapper<T> AsIList<T>(this ArraySegment<T> segment)
        {
            Contract.Ensures(Contract.Result<ArraySegmentListWrapper<T>>() != null);
            Contract.Ensures(Contract.Result<ArraySegmentListWrapper<T>>().Segment == segment);
            return new ArraySegmentListWrapper<T>(segment);
        }
 
        public static void CopyTo<T>(this ArraySegment<T> segment, ArraySegment<T> destination)
        {
            Array.Copy(segment.Array, segment.Offset, destination.Array, destination.Offset, Math.Min(segment.Count, destination.Count));
        }
  
        public static void CopyTo<T>(this ArraySegment<T> segment, T[] array, int arrayIndex = 0)
        {
            Contract.Requires(array != null);
            Contract.Requires(arrayIndex >= 0);
            Contract.Requires(segment.Count <= array.Length - arrayIndex);
            Array.Copy(segment.Array, segment.Offset, array, arrayIndex, segment.Count);
        }

        public static T[] ToArray<T>(this ArraySegment<T> segment)
        {
            Contract.Ensures(Contract.Result<T[]>() != null);
            Contract.Ensures(Contract.Result<T[]>().Length == segment.Count);
            var ret = new T[segment.Count];
            segment.CopyTo(ret);
            return ret;
        }
    }
}
    
