﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Encoders.Lame
{
    public enum LameResult : uint
    {
        Ok = 0x00000000,
        InvalidFormat = 0x00000001,
        InvalidFormatParams = 0x00000002,
        NoMoreHandles = 0X00000003,
        InvalidHandle = 0x00000004,
        BufferToSmall = 0x00000005,       
    }    

    public static class LameMp3Wrap
    {       
        [DllImport(@"lame_enc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern LameResult beInitStream(LameEncoderConfig config, ref uint samples, ref uint bufferSize, ref uint stream);

        [DllImport(@"lame_enc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern LameResult beDeinitStream(uint hbeStream, ref byte[] pOutput, uint pdwOutput);

        [DllImport(@"lame_enc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern LameResult beCloseStream(uint hbeStream);

        [DllImport(@"lame_enc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern LameResult beEncodeChunk(uint hbeStream, ulong nSamples, short[] pSamples, ref byte[] pOutput, ulong[] pdwOutput);
    }
}
