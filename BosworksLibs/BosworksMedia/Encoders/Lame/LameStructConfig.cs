﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Encoders.Lame
{
    public enum LameStructConfig : uint { Mp3  = 0, Lame = 256 }
    public enum MpegVersion : uint { Mpeg1 = 1, Mpeg2 = 0 }

    public enum ChannelMode : uint
    {
        Stereo       = 0,
        JointStereo  = 1,
        DualChannel  = 2,
        Mono         = 3
    }
    
    public enum VbrMethod : int
    {
        None    = -1,
        Default = 0,
        Old     = 1,
        New     = 2,
        Mtrh    = 3,
        Abr     = 4
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class LameEncoderConfig
    {
        public LameStructConfig dwConfig = LameStructConfig.Lame;
        public uint dwStructVersion = 1;
        public uint dwStructSize;

        public uint dwSampleRate = 44100;
        public uint dwResampleRate = 0;

        public ChannelMode nMode = ChannelMode.JointStereo;
        public uint dwBitrate = 128;
        public uint dwMaxBitrate = 0;
        public int nPreset = LamePresets.LQP_NORMAL_QUALITY;
        public MpegVersion dwMpegVersion = MpegVersion.Mpeg1;
        public uint dwPsyModel = 0;
        public uint dwEmphasis = 0;
        public uint bPrivate;
        public uint bCRC;
        public uint bCopyright;
        public uint bOriginal;
        public uint bWriteVBRHeader;
        public uint bEnableVBR;
        public int nVBRQuality;
        public uint dwVbrAbr_bps;
        public VbrMethod VBRMethod;
        public uint bNoRes;
        public uint bStrictIso;
        public ushort nQuality;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
        public byte[] btReserved = new byte[256];

        public LameEncoderConfig() { this.SetDefaults(); }
    }

    public static class LameConfigHelpers
    {
        public static LameEncoderConfig SetMp3Output(this LameEncoderConfig config, uint sampleRate, uint bitrate,
                                                     ChannelMode channels = ChannelMode.JointStereo, VbrMethod vbr = VbrMethod.None)
        {
            config.dwConfig = LameStructConfig.Lame;
            config.dwStructVersion = 1;
                               
            config.dwSampleRate = sampleRate;
            config.dwBitrate = bitrate;
            config.nMode = channels;
            config.VBRMethod = vbr;

            return config;
        }

        public static LameEncoderConfig SetDefaults(this LameEncoderConfig config)
        {
            config.dwConfig = LameStructConfig.Lame;
            config.dwStructVersion = 1;
            config.dwStructSize = (uint)Marshal.SizeOf(config);
            config.nPreset = LamePresets.LQP_NORMAL_QUALITY;
            config.dwSampleRate = 44100;
            config.dwResampleRate = 0;
            config.nMode = ChannelMode.JointStereo;
            config.dwBitrate = 128;
            config.dwMpegVersion = MpegVersion.Mpeg1;

            return config;
        }
    }


}
