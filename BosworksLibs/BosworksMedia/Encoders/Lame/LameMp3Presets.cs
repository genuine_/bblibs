﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Encoders.Lame
{
    public static class LamePresets
    {
        public const int LQP_NOPRESET = -1;
        public const int LQP_NORMAL_QUALITY = 0;
        public const int LQP_LOW_QUALITY = 1;
        public const int LQP_HIGH_QUALITY = 2;
        public const int LQP_VOICE_QUALITY = 3;
        public const int LQP_R3MIX = 4;
        public const int LQP_VERYHIGH_QUALITY = 5;
        public const int LQP_STANDARD = 6;
        public const int LQP_FAST_STANDARD = 7;
        public const int LQP_EXTREME = 8;
        public const int LQP_FAST_EXTREME = 9;
        public const int LQP_INSANE = 10;
        public const int LQP_ABR = 11;
        public const int LQP_CBR = 12;
        public const int LQP_MEDIUM = 13;
        public const int LQP_FAST_MEDIUM = 14;

        public const int LQP_PHONE = 1000;
        public const int LQP_SW = 2000;
        public const int LQP_AM = 3000;
        public const int LQP_FM = 4000;
        public const int LQP_VOICE = 5000;
        public const int LQP_RADIO = 6000;
        public const int LQP_TAPE = 7000;
        public const int LQP_HIFI = 8000;
        public const int LQP_CD = 9000;
        public const int LQP_STUDIO = 10000;
    }

}
