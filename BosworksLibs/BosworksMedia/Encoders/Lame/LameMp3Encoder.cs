﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Bosworks.Encoders.Lame;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Encoders.Lame
{
  
    public class LameMp3Encoder
    {            
        public LameEncoderConfig Config;

        public uint StreamId { get { return _streamId; } }
        public uint Samples { get { return _samples; } }
        public uint BufferSize { get { return _bufferSize; } }
        public LameResult LastResult { get { return _lastResult; } }

        private uint _streamId = 0;
        private uint _samples = 0;
        private uint _bufferSize = 0;
        private bool _initialized = false;
        private LameResult _lastResult;
      
        private void internalReset()
        {
            _streamId = _samples = _bufferSize = 0;
            _initialized = false;
            _lastResult = LameResult.Ok;
        }

        public virtual bool Initialize()
        {
            if (_initialized) return false;
            _lastResult = LameMp3Wrap.beInitStream(Config, ref _samples, ref _bufferSize, ref _streamId);
            _initialized = _lastResult == LameResult.Ok;
            return _initialized;
        }

        public virtual bool Close()
        {
            if (!_initialized) return false;
            _lastResult = LameMp3Wrap.beCloseStream(_streamId);        
            internalReset();            
            return true;
        }

        public LameMp3Encoder(LameEncoderConfig config) { Config = config; }

    }

}
