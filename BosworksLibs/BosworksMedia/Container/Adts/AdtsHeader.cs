﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ArraySegments;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{
    public enum AdtsMpegVersion : byte
    {
        MpegV25 = 0,
        MpegV4 = 1,
        MpegV2 = 2,
        MpegV1 = 3
    }

    public enum AdtsLayer : byte
    {
        AacProfile = 0,
        Layer3 = 1,
        Layer2 = 2,
        Layer1 = 3
    }

    public enum AdtsChannelMode : byte
    {
        Stereo = 0,
        JointStereo = 1,
        DualChannel = 2,
        Mono = 3
    }

    public enum AdtsAacProfile : byte
    {
        AacMain = 0,
        AacLc = 1,
        AacSsr = 2,
        AacLtp = 3
    }

    public enum AdtsAacChannelMode : byte
    {
        Aot = 0,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Eight = 7
    }

   
    public class AdtsHeader : IAdtsHeaderFactory
    {        
        public AdtsMpegVersion MpegVersion { get; set; }
        public AdtsLayer Layer { get; set; }
      
        public int Bitrate { get; set; }
        public int SampleRate { get; set; }

        public bool HasCrc { get; set; }
        public int FrameLen { get; set; }
        public int Padding { get; set; }
        public int SamplesPerFrame { get; set; }

        public bool ValidSync { get; set; }
        public int Length { get; set; } 

        public virtual bool CheckSyncBytes(ArraySegment<byte> srcBytes)
        {
            if (srcBytes.Count < 7) throw new ArgumentException("AdtsHeader - HEY! Data segment too small", "srcBytes");
 
            ValidSync = (srcBytes.Array[srcBytes.Offset] == 0xFF) && ((srcBytes.Array[srcBytes.Offset + 1] & 0xF0) == 0xF0);
            if (!ValidSync)
            {
                Length = 0;
                return false;
            }
            return true;
        }
        
        public virtual bool LoadFrom(ArraySegment<byte> srcBytes)
        {
            if (!CheckSyncBytes(srcBytes)) return false;
          
            MpegVersion = (AdtsMpegVersion)((srcBytes.Array[srcBytes.Offset + 1] & 0x18) >> 3);
            Layer =       (AdtsLayer)      ((srcBytes.Array[srcBytes.Offset + 1] & 0x06) >> 1);

            HasCrc =      (srcBytes.Array[srcBytes.Offset + 1] & 0x01) == 0;
            Length =      (HasCrc ? 9 : 7);
            return true;
        }

        public AdtsHeader() {}
        public AdtsHeader(ArraySegment<byte> srcBytes) { LoadFrom(srcBytes); }

        // Calling this base factory method will attempt mpeg auto select
        public virtual AdtsHeader CreateInstance(ArraySegment<byte> data)
        {
            return AdtsHeaderFactory.AutoDetectHeader(data);
        }

        public virtual AdtsHeader CreateInstance()
        {
         	return new AdtsHeader();
        }
    }
   

    public static class AdtsStaticData
    {
        public static readonly int[,] MpegV1Bitrates = new int[4,16]
        {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1}, /* Reserved */
            {0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, -1},    /* layer 3 */
            {0, 32, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384, -1},   /* layer 2 */
            {0, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448, -1} /* layer 1 */
        };

        public static readonly int[] AacSampleRates = new int[16]
        {
             96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350, -1, -1, 0 
        };

        public static readonly int[] MpegV1SampleRates  = new int[4] {44100, 48000, 32000, -1};
        public static readonly int[] MpegV2SampleRates  = new int[4] {22050, 24000, 16000, -1};
        public static readonly int[] MpegV25SampleRates = new int[4] {11025, 12000, 8000,  -1};
        
    }

}
