﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArraySegments;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{
    public interface IAdtsHeaderFactory
    {
        AdtsHeader CreateInstance();
        AdtsHeader CreateInstance(ArraySegment<byte> data);
    }

    public struct AdtsHeaderType : IEquatable<AdtsHeaderType>
    {
        private readonly AdtsMpegVersion _version;
        private readonly AdtsLayer _layer;

        public AdtsMpegVersion Version { get { return _version; } }
        public AdtsLayer Layer { get { return _layer; } }

        public AdtsHeaderType(AdtsMpegVersion ver, AdtsLayer layer)
        {
            _version = ver;
            _layer = layer;
        }

        public override int GetHashCode()
        {
            var hash = 17;
            hash = hash * 31 + Version.GetHashCode();
            hash = hash * 31 + Layer.GetHashCode();
            return hash;
        }

        public override bool Equals(object other)
        {
            return other is AdtsHeaderType && Equals((AdtsHeaderType)other);
        }

        public bool Equals(AdtsHeaderType other)
        {
            return Version == other.Version &&
                   Layer == other.Layer;
        }
    }

    public static class AdtsHeaderFactory
    {
        public static Dictionary<AdtsHeaderType, IAdtsHeaderFactory> Bindings;

        public static IAdtsHeaderFactory GetFactoryByMpegType(AdtsMpegVersion ver, AdtsLayer layer)
        {
            var hdr = new AdtsHeaderType(ver, layer);

            IAdtsHeaderFactory header = null;
            Bindings.TryGetValue(hdr, out header);
            return header ?? null;
        }

        public static AdtsHeader AutoDetectHeader(ArraySegment<byte> srcBytes)
        {
            if (srcBytes.Count < 7) throw new ArgumentException("AdtsHeader - HEY! Data segment too small", "srcBytes");
            var data = srcBytes.Take(7);

            var validSync = (data.Array[data.Offset] == 0xFF) && ((data.Array[data.Offset + 1] & 0xF0) == 0xF0);
            if (!validSync) return null;

            var mpegVersion = (AdtsMpegVersion)((data.Array[data.Offset + 1] & 0x18) >> 3);
            var layer = (AdtsLayer)((data.Array[data.Offset + 1] & 0x06) >> 1);

            var factory = GetFactoryByMpegType(mpegVersion, layer);
            if (factory == null) return null;
            
            var hdr = factory.CreateInstance();
            hdr.LoadFrom(srcBytes);
            return hdr;            
        }

        static AdtsHeaderFactory()
        {
            Bindings = new Dictionary<AdtsHeaderType, IAdtsHeaderFactory>();

            var combo = new AdtsHeaderType(AdtsMpegVersion.MpegV1, AdtsLayer.Layer3);
            Bindings.Add(combo, new AdtsMp3Header());

            combo = new AdtsHeaderType(AdtsMpegVersion.MpegV1, AdtsLayer.Layer2);
            Bindings.Add(combo, new AdtsMp3Header());

            combo = new AdtsHeaderType(AdtsMpegVersion.MpegV1, AdtsLayer.Layer1);
            Bindings.Add(combo, new AdtsMp3Header());

            combo = new AdtsHeaderType(AdtsMpegVersion.MpegV2, AdtsLayer.AacProfile);
            Bindings.Add(combo, new AdtsAacHeader());

            combo = new AdtsHeaderType(AdtsMpegVersion.MpegV4, AdtsLayer.AacProfile);
            Bindings.Add(combo, new AdtsAacHeader());
        }
    }

}
