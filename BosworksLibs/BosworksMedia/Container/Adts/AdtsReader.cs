﻿using System;
using System.Collections.Concurrent;
using ArraySegments;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts{

    public delegate void AdtsPacketEvent(object sender, AdtsPacket packet);
    public delegate void AdtsSyncStateEvent(object sender, int pos, AdtsParseState state);
    public delegate void AdtsSkipData(object sender, byte[] dataIn, int ofs, int count);

    public class AdtsReader : AdtsReaderBase
    {
        public event AdtsPacketEvent OnAdtsPacket = null;
        public event AdtsSyncStateEvent OnSyncState = null;
        public event AdtsSkipData OnSkipData = null;

        protected override void notifyPacketReady(AdtsPacket newPacket)
        {
            if (OnAdtsPacket != null) OnAdtsPacket(this, newPacket);
        }

        protected override void notifySkippedData(byte[] dataIn, int startIdx, int count)
        {
            if (OnSkipData != null) OnSkipData(this, dataIn, startIdx, count);
        }

        protected override void notifySyncStateChange(int pos, AdtsParseState state)
        {
            if (OnSyncState != null) OnSyncState(this, pos, state);
        }

        public AdtsReader(AdtsCaptureMode capMode, IAdtsHeaderFactory hdrFactory = null) : base(capMode, hdrFactory)
        {
        }
    }
}
