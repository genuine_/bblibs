﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArraySegments;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{

    public class AdtsAacHeader : AdtsHeader
    {
        public AdtsAacProfile Profile { get; set; }
        public AdtsAacChannelMode AacChannelMode { get; set; }

        public override bool LoadFrom(ArraySegment<byte> srcBytes)
        {            
            if (!base.LoadFrom(srcBytes)) return false;

            var ver = ((srcBytes.Array[srcBytes.Offset + 1] & 0x08) >> 3);
            if (ver == 0) MpegVersion = AdtsMpegVersion.MpegV4;
            if (ver == 1) MpegVersion = AdtsMpegVersion.MpegV2;
            
            Profile = (AdtsAacProfile)((srcBytes.Array[srcBytes.Offset + 2] & 0xc0) >> 6);

            var aacb = (srcBytes.Array[srcBytes.Offset + 2] & 0x3c) >> 2;
            SampleRate = AdtsStaticData.AacSampleRates[aacb];

            aacb = ((srcBytes.Array[srcBytes.Offset + 2] & 0x01) << 2) + ((srcBytes.Array[srcBytes.Offset + 3] & 0xc0) >> 6);
            AacChannelMode = (AdtsAacChannelMode)aacb;

            aacb = ((srcBytes.Array[srcBytes.Offset + 3] & 0x02) << 11) + ((srcBytes.Array[srcBytes.Offset + 4]) << 3) + ((srcBytes.Array[srcBytes.Offset + 5] & 0xe0) >> 5);
            FrameLen = aacb;
            return true;
        }

        public override AdtsHeader CreateInstance(ArraySegment<byte> data)
        {
            var hdr = new AdtsAacHeader();
            hdr.LoadFrom(data);
            return hdr;
        }

        public override AdtsHeader CreateInstance()
        {
            return new AdtsAacHeader();
        }
    }

}
