﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

/* Brian Bosworth  (bbosworth76@gmail.com)      (╯°□°)-︻╦╤─ - - -         */

namespace Bosworks.Container.Adts
{
    [StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2)]
    public struct AdtsSyncStruct
    {
        [FieldOffset(0)] public byte Sync;
        [FieldOffset(1)] public byte VersionAndProtection;
    }

    public struct AdtsSyncFrame
    {
        public AdtsSyncStruct DataBytes;

        public int DataIndex;
        public int HeaderLen;

        public bool ValidSync
        {
            get { return (DataBytes.Sync == 0xff) && (DataBytes.VersionAndProtection & 0xf0) == 0xf0; }
        }


        // todo: there is a boundary case failure for sync frame start at the last byte
        public static AdtsSyncFrame FindSync(byte[] dataIn, int ofs, int count)
        {
            for (var x = ofs; x < ofs + count - 1; x++)
            {
                if ((dataIn[x] == 0xff) && (dataIn[x + 1] & 0xf0) > 0) return new AdtsSyncFrame(x, dataIn[x], dataIn[x + 1]);
            }
            return new AdtsSyncFrame(-1, 0, 0);
        }

        public AdtsSyncFrame(int idx, byte sync, byte vandc)
        {
            DataIndex = idx;
            DataBytes.Sync = sync;
            DataBytes.VersionAndProtection = vandc;
            HeaderLen = 0;
            if (ValidSync) HeaderLen = (DataBytes.VersionAndProtection & 0x01) == 1 ? 7 : 9;
        }
    }
}
